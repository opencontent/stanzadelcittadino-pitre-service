import json
from time import sleep

import requests
import datetime
from urllib.parse import urlencode
from utils.logger import logger

class PiTreException(Exception):
    pass


class PiTre:
    def __init__(self, username, certificate, api_url, code_role, code_register, code_application, code_adm,
                 means_of_sending, attempts=10, sleep_seconds=60, connection_timeout=10, read_timeout=300, transmission_reason=None, code_rf=None,
                 code_project=None, private_project=None, private_document=None,
                 default_sender=None,
                 transmission_project_models=None, transmission_document_models=None, code_node_classification=None,
                 receiver_code=None, default_note=None, address_book_rf=None, project_template_id=None,
                 income_document_template_id=None, outcome_document_template_id=None):

        if transmission_document_models is None:
            transmission_document_models = []
        if not isinstance(transmission_document_models, list):
            raise PiTreException("PiTre: transmission_document_model must be a list: got \"{}\"".format(
                transmission_document_models
            ))

        if transmission_project_models is None:
            transmission_project_models = []
        if not isinstance(transmission_project_models, list):
            raise PiTreException("PiTre: transmission_project_model must be a list: got \"{}\"".format(
                transmission_project_models
            ))

        if not private_project:
            private_project = False
        if not private_document:
            private_document = False

        # requests config
        self.attempts = attempts
        self.sleep_seconds = sleep_seconds
        self.connection_timeout = connection_timeout
        self.read_timeout = read_timeout

        self.token_time = None
        self.token = None
        self.classification_scheme_id = None
        self.code_node_classification = code_node_classification
        self.default_sender = None
        self.certificate = certificate
        self.api_url = api_url
        self.code_project = code_project
        self.username = username
        self.code_role = code_role
        self.code_application = code_application
        self.code_register = code_register
        self.code_adm = code_adm
        self.means_of_sending = means_of_sending
        self.transmission_project_models = transmission_project_models
        self.transmission_document_models = transmission_document_models
        self.transmission_reason = transmission_reason
        self.code_rf = code_rf
        self.receiver_code = receiver_code
        self.default_note = default_note
        self.address_book_rf = address_book_rf
        self.project_template_id = project_template_id
        self.project_template = None
        self.income_document_template_id = income_document_template_id
        self.income_document_template = None
        self.outcome_document_template_id = outcome_document_template_id
        self.outcome_document_template = None
        self.private_project = private_project
        self.private_document = private_document

        self.set_token()
        self.set_active_classification_scheme()

        # Ricerca mittente di default
        if default_sender:
            self.set_default_sender(default_sender)

        # Modelli di trasmissione
        for project_model in self.transmission_project_models:
            self.check_transmission_model(project_model)
        for document_model in self.transmission_document_models:
            self.check_transmission_model(document_model)

        # Fascicolo tipizzato
        if project_template_id:
            self.set_project_template()
        # Documento tipizzato
        if income_document_template_id:
            self.set_document_template(income=True)
        if outcome_document_template_id:
            self.set_document_template(income=False)

    def set_default_sender(self, default_sender):
        """
        Metodo per la ricerca del mittente di default dei protocolli in uscita e destinatario dei protocolli in ingresso
        """
        status, response = self.search_correspondence(
            national_identification_number=None,
            code_role=default_sender['code_role'] if "code_role" in default_sender else None,
            type=default_sender['type'] if "type" in default_sender else None,
            offices=default_sender['offices'] if "type" in default_sender else None,
            roles=default_sender['roles'] if "type" in default_sender else None,
            users=None
        )
        if status == 1 or len(response) == 0:
            raise PiTreException("PiTre: No sender found for {}".format(json.dumps(default_sender, indent=2)))
        else:
            self.default_sender = response[0]

    def set_project_template(self):
        status, response = self.get_template_project(self.project_template_id)
        if status == 1 or len(response) == 0:
            raise PiTreException("PiTre: No project template found with id {}".format(self.project_template_id))
        else:
            self.project_template = response

    def set_document_template(self, income=True):
        if not income and self.outcome_document_template_id:
            status, response = self.get_template_document(self.outcome_document_template_id)
        else:
            status, response = self.get_template_document(self.income_document_template_id)
        if status == 1 or len(response) == 0:
            raise PiTreException(
                "PiTre: No document template found with id {}".format(self.income_document_template_id))
        else:
            if not income and self.outcome_document_template_id:
                self.outcome_document_template = response
            else:
                self.income_document_template = response

    def get_document_template(self, income=True):
        if not income and self.outcome_document_template:
            return self.outcome_document_template
        else:
            return self.income_document_template

    def get_default_sender(self):
        return self.default_sender

    def set_token(self):
        """
        Metodo per la generazione del token di autenticazione da inserire nelle successive chiamate, in un header
        "AuthToken"
        """

        data = {
            "Username": self.username,
            "CodeRole": self.code_role,
            "CodeApplication": self.code_application,
            "CodeAdm": self.code_adm
        }

        response = self.safe_request(method="POST", route_name="GetToken", is_token_required=False, data=data)

        if response['Code'] == 1:
            raise PiTreException("PiTre: Invalid credentials {}".format(json.dumps(data, indent=2)))
        else:
            self.token = response['Token']
            self.token_time = datetime.datetime.now()

    def get_token(self):
        if (datetime.datetime.now() - self.token_time).seconds > 60 * 10:
            self.set_token()
        return self.token

    def set_active_classification_scheme(self):
        """
        Metodo per il prelievo del titolario attivo, all'interno del quale possono essere creati i nuovi fascicoli
        """
        response = self.safe_request(method="GET", route_name="GetActiveClassificationScheme")

        if response['Code'] == 1:
            self.classification_scheme_id = None
        else:
            self.classification_scheme_id = response['ClassificationScheme']['Id']

    def get_template_project(self, project_template_id):
        """
        Servizio per il reperimento del dettaglio di una tipologia di fascicolo dato l’id.
        :param id_template: Id della tipologia di fascicolo
        :return: Template
        """
        project_template_id = project_template_id or self.project_template_id

        response = self.safe_request(method="GET", route_name="GetTemplateProject", idTemplate=project_template_id)
        return response['Code'], response['Template'] if response['Code'] == 0 else response['ErrorMessage']

    def get_template_document(self, document_template_id):
        """
        Servizio per il reperimento del dettaglio di una tipologia di documento dato l’id.
        :param document_template_id: Id del template del documento
        :param id_template: Id della tipologia di documento
        :return: Template
        """
        document_template_id = document_template_id

        response = self.safe_request(method="GET", route_name="GetDocumentTemplate", idTemplate=document_template_id)
        return response['Code'], response['Template'] if response['Code'] == 0 else response['ErrorMessage']

    def create_project(self, description, template=None, classification_scheme=None, code_node_classification=None, private=None):
        """
        Metodo che permette la creazione di un fascicolo.
        :param template: Fascicolo tipizzato
        :param code_node_classification: Nodo del titolario nel quale creare il fascicolo
        :param description: Descrizione del fascicolo.
        :param classification_scheme: Id del titolario oin cui creare il fascicolo.
        :param private: Fascicolo privato?
        :return Progetto creato
        """
        code_node_classification = code_node_classification or self.code_node_classification
        classification_scheme = classification_scheme or self.classification_scheme_id
        private = private or self.private_project

        data = {
            "Project": {
                "Description": description,
                "ClassificationScheme": {
                    "Id": classification_scheme,
                },
                "Private": private,
                "Template": template,
                "CodeNodeClassification": code_node_classification,
            }
        }

        response = self.safe_request(method="PUT", route_name="CreateProject", data=data)
        return response['Code'], response['Project'] if response['Code'] == 0 else response['ErrorMessage']

    def get_transmission_models(self, type, registers):
        """
        Metodo per il reperimento di tutti i modelli di trasmissione per documenti o fascicoli.
        :param type: Inserire “D” per i modelli dei documenti, “F” per i modelli dei fascicoli.
        :param registers:  Array di oggetti Register. Obbligatorio almeno un codice di un registro.
        :return: Restituisce un messaggio di avvenuta trasmissione in caso di successo.
        """
        data = {
            "Type": type,
            "Registers": registers
        }
        response = self.safe_request(method="POST", route_name="GetTransmissionModels", data=data)
        return response['Code'], response['TransmissionModels'] if response['Code'] == 0 else response['ErrorMessage']

    def check_transmission_model(self, id_model):
        """
        Metodo per la verifica dell'esistenza di un dato modello di trasmission
        :param id_model: id del modello di trasmissione
        :return: Solleva un eccezione nel caso in cui il modello non esista
        """
        status, response = self.get_transmission_model_by_id(id_model)
        if status == 1:
            raise PiTreException("PiTre: Invalid transmission model {}".format(id_model))

    def get_transmission_model_by_id(self, id_model):
        """
        Servizio per il reperimento del dettaglio di un modello di trasmissione dato l’id del modello.
        :return: Modello di trasmissione
        """
        response = self.safe_request(method="GET", route_name="GetTransmissionModel", idModel=id_model)
        return response['Code'], response['TransmissionModel'] if response['Code'] == 0 else response['ErrorMessage']

    def get_transmission_model_by_code(self, code_model):
        """
        Servizio per il reperimento del dettaglio di un modello di trasmissione dato il codice del modello.
        :return: Modello di trasmissione
        """
        response = self.safe_request(method="GET", route_name="GetTransmissionModel", codeModel=code_model)
        return response['Code'], response['TransmissionModel'] if response['Code'] == 0 else response['ErrorMessage']

    def execute_transm_prj_model(self, id_project, id_model):
        """
        Metodo per la trasmissione di un fascicolo tramite un modello di trasmissione.
        :param id_model: Id del modello di trasmissione
        :param id_project:  Id del fascicolo da trasmettere.
        :return: Restituisce un messaggio di avvenuta trasmissione in caso di successo.
        """
        data = {
            "IdModel": id_model,
            "IdProject": id_project
        }
        response = self.safe_request(method="POST", route_name="ExecuteTransmPrjModel", data=data)
        return response['Code'], response['TransmMessage'] if response['Code'] == 0 else response['ErrorMessage']

    def execute_transm_doc_model(self, id_document, id_model):
        """
        Metodo per la trasmissione di un documento tramite un modello di trasmissione.
        :param id_model: Id del modello di trasmissione
        :param id_document:  Id del documento da trasmettere.
        :return: Restituisce un messaggio di avvenuta trasmissione in caso di successo.
        """
        data = {
            "IdModel": id_model,
            "DocumentId": id_document
        }
        response = self.safe_request(method="POST", route_name="ExecuteTransmDocModel", data=data)
        return response['Code'], response['TransmMessage'] if response['Code'] == 0 else response['ErrorMessage']

    def get_registers_or_rf(self, code_role=None):
        """
        Metodo utilizzato per il prelievo del dettaglio dei registri o RF disponibili per un ruolo.
        :param code_role: Codice del ruolo
        :return: Lista dei registri o None se si è verificato un errore
        """
        code_role = code_role or self.code_role
        response = self.safe_request(method="GET", route_name="GetRegistersOrRF", codeRole=code_role)
        return response['Code'], response['Registers'] if response['Code'] == 0 else response['ErrorMessage']

    def get_users_in_role(self, code_role=None):
        """
        Metodo per il prelievo degli utenti presenti in un determinato ruolo, a partire dal codice del ruolo.
        :param code_role: Codice del ruolo
        :return: lista degli utenti
        """
        code_role = code_role or self.code_role
        response = self.safe_request(method="GET", route_name="GetUsersInRole", codeRole=code_role)
        return response['Code'], response['Users'] if response['Code'] == 0 else response['ErrorMessage']

    def get_project(self, code_project=None):
        """
        Servizio per il reperimento dei dati di un fascicolo dato un codice o l’id.
        :param code_project: Codice del fascicolo
        :return: Dati di un fascicolo
        """
        code_project = code_project or self.code_project
        response = self.safe_request(
            method="GET",
            route_name="GetProject",
            codeProject=code_project,
            classificationSchemeId=self.classification_scheme_id
        )
        return response['Code'], response['Project'] if response['Code'] == 0 else response['ErrorMessage']

    def upload_file_to_document(self, document_id, base64_content, original_name, description, create_attachment=True):
        """
        Metodo per aggiungere un file ad un documento, per creare un allegato o per aggiungere una nuova versione del file.
        :param document_id: DocNumber del documento.
        :param base64_content: File da acquisire in formato base64
        :param original_name: Nome del file, con estensione. Il nome deve contenere solo caratteri accettati da file
                system, e deve avere un'estensione ammessa in amministrazione.
        :param description: Descrizione del file da acquisire.
        :param create_attachment: Flag per indicare se il documento che si vuole caricare è un allegato oppure il documento
                principale (Default: true)
        """
        data = {
            "IdDocument": document_id,
            "File": {
                "Content": base64_content.decode('utf-8'),
                "Name": original_name.replace(" ", "")
            },
            "CreateAttachment": create_attachment,
            "Description": description
        }
        response = self.safe_request(method="PUT", route_name="UploadFileToDocument", data=data)
        return response['Code'], response['ResultMessage'] if response['Code'] == 0 else response['ErrorMessage']

    def search_correspondence(self, national_identification_number=None, code_role=None, type=None, offices=None,
                              roles=None, users=None, address_book_rf=False):
        """
        Metodo per la ricerca dei corrispondenti, inserendo nella richiesta i filtri disponibili tramite il metodo GetCorrespondentFilters
        :param code_role:
        :param national_identification_number: Filtro utilizzato per la ricerca di corrispondenti indicando il codice fiscale.
        :return: Corrispondente
        """
        data = {
            "Filters": []
        }

        if national_identification_number:
            data["Filters"].append({
                "Name": "NATIONAL_IDENTIFICATION_NUMBER",
                "Value": national_identification_number,
            })
        if users:
            data["Filters"].append({"Name": "USERS", "Value": users})
        if code_role:
            data["Filters"].append({"Name": "EXACT_CODE", "Value": code_role})
        if type:
            data["Filters"].append({"Name": "TYPE", "Value": type})
        if offices:
            data["Filters"].append({"Name": "OFFICES", "Value": offices})
        if roles:
            data["Filters"].append({"Name": "ROLES", "Value": roles})
        if address_book_rf and self.address_book_rf:
            data["Filters"].append({"Name": "REGISTRY_OR_RF", "Value": self.address_book_rf})

        response = self.safe_request(method="POST", route_name="SearchCorrespondents", data=data)
        return response['Code'], response['Correspondents'] if response['Code'] == 0 else response['ErrorMessage']

    def add_correspondent(self, name, surname, description, code, correspondent_type="P", address=None, cap=None,
                          city=None, province=None, location=None, nation=None, phone_number=None, phone_number2=None,
                          fax=None, national_identification_number=None, vat_number=None, email=None, other_emails=None,
                          AOO_code=None, adm_code=None, note=None, type=None,
                          preferred_channel="MAIL", is_common_address=True):
        """
        Servizio per la creazione di un nuovo corrispondente esterno.
        :param name: nome
        :param surname: cognome
        :param description: Obbligatorio. Descrizione del corrispondente.
        :param code:  Obbligatorio. Codice del corrispondente ricercabile da rubrica.
        :param correspondent_type: Obbligatorio. Tipologia del corrispondente. Può assumere i valori “U” nel caso di
                                    Unità organizzativa e “P” nel caso di persona.
        :param address: Opzionale. Indirizzo del corrispondente.
        :param cap:  Opzionale. Cap del corrispondente.
        :param city: Opzionale. Città del corrispondente.
        :param province: Opzionale. Provincia del corrispondente.
        :param location: Opzionale. Località del corrispondente.
        :param nation: Opzionale. Nazione del corrispondente.
        :param phone_number:  Opzionale. Numero di telefono del corrispondente.
        :param phone_number2: Opzionale. Secondo numero di telefono del corrispondente.
        :param fax:  Opzionale. Numero del fax del corrispondente.
        :param national_identification_number:  Opzionale. Codice fiscale del corrispondente.
        :param vat_number:
        :param email: Opzionale. Mail del corrispondente.
        :param other_emails: lista di indirizzi email alternativi
        :param AOO_code: Opzionale. Codice AOO.
        :param adm_code: Opzionale. Codice amministrazione.
        :param note: Opzionale. Note
        :param type: tipologia
        :param preferred_channel: Opzionale. Canale preferenziale del corrispondente.
        :param is_common_address:
        :return: Corrispondente creato
        """
        data = {
            "Correspondent": {
                "Description": description,
                "Code": code,
                "Address": address,
                "Cap": cap,
                "City": city,
                "Province": province,
                "Location": location,
                "Nation": nation,
                "PhoneNumber": phone_number,
                "PhoneNumber2": phone_number2,
                "Fax": fax,
                "NationalIdentificationNumber": national_identification_number,
                "VatNumber": vat_number,
                "Email": email,
                "OtherEmails": other_emails or [],
                "AOOCode": AOO_code,
                "AdmCode": adm_code,
                "Note": note,
                "Type": type,
                "CodeRegisterOrRF": self.address_book_rf or None,
                "CorrespondentType": correspondent_type,
                "PreferredChannel": preferred_channel,
                "Name": name,
                "Surname": surname,
                "IsCommonAddress": is_common_address
            }
        }
        response = self.safe_request(method="PUT", route_name="AddCorrespondent", data=data)
        return response['Code'], response['Correspondent'] if response['Code'] == 0 else response['ErrorMessage']

    def search_projects(self, project_description=None, year=None, project_number=None, project_code=None,
                        template=None,
                        page_number=1, elements_in_page=1):
        """
        Servizio che permette la ricerca di fascicoli
        :param elements_in_page: Numero di elementi per pagina
        :param page_number: Numero della pagine
        :param project_code: Filtro che permette la ricerca per codice di fascicolo.
        :param project_number: Filtro utilizzato per cercare il fascicolo per numero.
        :param year: Filtro per inserire l’anno.
        :param project_description: Filtro utilizzato per cercare i fascicoli per descrizione.
        :return: Fascicolo
        """
        data = {
            "Filters": [],
            "PageNumber": page_number,
            "ElementsInPage": elements_in_page
        }

        if project_description:
            data["Filters"].append(
                {
                    "Name": "PROJECT_DESCRIPTION",
                    "Value": str(project_description)
                }
            )

        if year:
            data["Filters"].append(
                {
                    "Name": "YEAR",
                    "Value": str(year)
                }
            )

        if project_number:
            data["Filters"].append(
                {
                    "Name": "PROJECT_NUMBER",
                    "Value": str(project_number)
                }
            )

        if project_code:
            data["Filters"].append(
                {
                    "Name": "PROJECT_CODE",
                    "Value": str(project_code)
                }
            )

        if template:
            data["Filters"].append(template)

        response = self.safe_request(method="POST", route_name="SearchProjects", data=data)
        return response['Code'], response['Projects'] if response['Code'] == 0 else response['ErrorMessage']

    def search_document(self, in_protocol, document_description=None, predisposed=True, attachments=False,
                        code_register=None, template=None, year=None, page_number=1, elements_in_page=1):
        """
        Metodo per la ricerca dei documenti
        :param year: Ricerca per anno
        :param template: Ricerca temlate
        :param code_register: codice registro
        :param attachments: Ottieni allegati?
        :param predisposed: Predisposto?
        :param in_protocol: protocollo in entrata?
        :param document_description: Filtro utilizzato per cercare i documenti per oggetto.
        :param elements_in_page: Numero di elementi per pagina
        :param page_number: Numero della pagine
        :return: Fascicolo
        """
        code_register = code_register or self.code_register
        data = {
            "Filters": [
                {
                    "Name": "REGISTER",
                    "Value": str(code_register)
                },
                {
                    "Name": "PREDISPOSED",
                    "Value": "true" if predisposed else "false"
                },
                {
                    "Name": "ATTACHMENTS",
                    "Value": "true" if attachments else "false"
                }
            ],
            "PageNumber": page_number,
            "ElementsInPage": elements_in_page
        }

        if document_description:
            data["Filters"].append(
                {
                    "Name": "OBJECT",
                    "Value": str(document_description)
                }
            )

        if template:
            data["Filters"].append(template)

        if in_protocol:
            data['Filters'].append(
                {
                    "Name": "IN_PROTOCOL",
                    "Value": "true"
                }
            )
        else:
            data['Filters'].append(
                {
                    "Name": "OUT_PROTOCOL",
                    "Value": "true"
                }
            )

        if year:
            data['Filters'].append(
                {
                    "Name": "YEAR",
                    "Value": str(year)
                }
            )

        response = self.safe_request(method="POST", route_name="SearchDocuments", data=data)
        return response['Code'], response['Documents'] if response['Code'] == 0 else response['ErrorMessage']

    def send_document_by_id(self, document_id):
        """
        Servizio per l’invio di un documento.
        :param document_id: Id del documento
        :return: Esito della chiamata
        """
        data = {
            "IdDocument": document_id,
        }
        response = self.safe_request(method="POST", route_name="SendDocument", data=data)
        return response['Code'], response['ResultMessage'] if response['Code'] == 0 else response['ErrorMessage']

    def send_document_by_signature(self, signature):
        """
        Servizio per l’invio di un documento.
        :param signature: Segnatura del documento
        :return: Esito della chiamata
        """
        data = {
            "Signature": signature,
        }
        response = self.safe_request(method="POST", route_name="SendDocument", data=data)
        return response['Code'], response['ResultMessage'] if response['Code'] == 0 else response['ErrorMessage']

    def get_role(self, code_role):
        response = self.safe_request(method="GET", route_name="GetRole", codeRole=code_role)
        return response['Code'], response['Role'] if response['Code'] == 0 else response['ErrorMessage']

    def get_correspondence(self):
        """
        Metodo che restituisce la lista dei filtri applicabili alla ricerca corrispondenti.
        :return: lista dei filtri applicabili alla ricerca corrispondenti.
        """
        response = self.safe_request(method="GET", route_name="GetCorrespondentFilters")
        return response['Code'], response['Correspondents'] if response['Code'] == 0 else response['ErrorMessage']

    def get_project_filters(self):
        """
        Metodo che restituisce la lista dei filtri applicabili alla ricerca fascicoli.
        :return: lista dei filtri applicabili alla ricerca fascicoli.
        """
        response = self.safe_request(method="GET", route_name="GetProjectFilters")
        return response['Code'], response['Filters'] if response['Code'] == 0 else response['ErrorMessage']

    def create_occasional_correspondent(self, description):
        return {
            "Description": description,
            "Type": "O",
            "CorrespondentType": "O"
        }

    def create_document(self, object, sender, document_type, template=None, recipient=None, arrival_date=None,
                        note=False, private=None):
        private = private or self.private_document
        document_note = None
        if note:
            document_note = [
                {
                    "Description": self.default_note
                }
            ]

        data = {
            "CodeRegister": self.code_register,
            "Document": {
                "Object": object,
                "DocumentType": document_type,
                "Predisposed": True,
                "MeansOfSending": self.means_of_sending,
                "Sender": sender,
                "Template": template,
                "Recipients": [recipient],
                "ArrivalDate": arrival_date.strftime("%d/%m/%Y %H:%M:%S") if arrival_date else None,
                "DataProtocolSender": arrival_date.strftime(
                    "%d/%m/%Y %H:%M:%S") if arrival_date else None,
                "Note": document_note,
                "PrivateDocument": private
            },
            "CodeRF": self.code_rf
        }

        response = self.safe_request(method="PUT", route_name="CreateDocument", data=data)
        return response['Code'], response['Document'] if response['Code'] == 0 else response['ErrorMessage']

    def get_document(self, document_id):
        """
        Metodo per il reperimento del dettaglio di un documento dato l’id.
        :param document_id: Id del documento
        :return: Documento richiesto
        """
        data = {
            "IdDocument": document_id,
        }
        response = self.safe_request(method="POST", route_name="GetDocument", data=data)
        return response['Code'], response['Document'] if response['Code'] == 0 else response['ErrorMessage']

    def edit_document(self, document_id, code_rf=None):
        """
        Metodo per la modifica di un documento o un protocollo. La richiesta è nella stessa forma di CreateDocument.
        :param document_id: id del documento
        :param code_rf: Codice dell’RF per la segnatura.
        :return: Documento modificato
        """
        code_rf = code_rf or self.code_rf
        status, document = self.get_document(document_id)
        if status == 1:
            logger.error('Il documento richiesto non esiste')
            return None

        document['Predisposed'] = False
        data = {
            "Document": document,
            "CodeRF": code_rf
        }
        response = self.safe_request(method="POST", route_name="EditDocument", data=data)
        return response['Code'], response['Document'] if response['Code'] == 0 else response['ErrorMessage']

    def add_doc_in_project(self, document_id, code_project=None):
        """
        Metodo per l'inserimento di un documento in un fascicolo.
        :param document_id: id del documento
        """
        code_project = code_project or self.code_project
        data = {
            "IdDocument": document_id,
            "CodeProject": code_project
        }
        response = self.safe_request(method="POST", route_name="AddDocInProject", data=data)
        return response['Code'], response['ResultMessage'] if response['Code'] == 0 else response['ErrorMessage']

    def execute_transmission_project(self, project_id, receiver_code=None, transmission_reason=None):
        """
        Metodo per la trasmissione singola di un fascicolo senza l’utilizzo di un modello di trasmissione.
        :param transmission_reason: Codice della ragione di trasmissione con la quale si invia il documento.
        :param receiver_code: Destinatario della trasmissione.
        :param project_id: Id del fascicolo che si desidera trasmettere.
        """
        receiver_code = receiver_code or self.receiver_code
        transmission_reason = transmission_reason or self.transmission_reason

        data = {
            "IdProject": project_id,
            "TransmissionReason": transmission_reason,
            "Receiver": {
                "Code": receiver_code
            },
            "Notify": True,
            "TransmissionType": "S"
        }
        response = self.safe_request(method="POST", route_name="ExecuteTransmissionProject", data=data)
        return response['Code'], response['TransmMessage'] if response['Code'] == 0 else response['ErrorMessage']

    def protocol_predisposed(self, code_register, document_id, code_rf=None):
        """
        Metodo per la protocollazione di un documento predisposto senza modificare ulteriori dati
        :param document_id: l'id del documento predisposto
        :param code_register: registro di protocollazione
        :param code_rf: 'eventuale RF.
        """
        code_rf = code_rf or self.code_rf

        data = {
            "CodeRegister": code_register,
            "CodeRF": code_rf,
            "IdDocument": document_id
        }
        response = self.safe_request(method="POST", route_name="ProtocolPredisposed", data=data)
        return response['Code'], response['Document'] if response['Code'] == 0 else response['ErrorMessage']

    def get_classification_scheme_by_id(self, id_classification_scheme):
        """
        Servizio per il reperimento dei dettagli di un titolario dato l’id.
        :param id_classification_scheme: Id del titolario cercato.
        :return: Titolario
        """
        response = self.safe_request(
            method="GET",
            route_name="GetClassificationSchemeById",
            idClassificationScheme=id_classification_scheme
        )
        return response['Code'], response['ClassificationScheme'] if response['Code'] == 0 else response['ErrorMessage']

    def get_timestamp_and_signature(self, id_document=None, signature=None):
        if not (id_document or signature):
            raise PiTreException(f"PiTre: missing document id or signature in GetStampAndSignature")

        response = None
        if id_document:
            response = self.safe_request("GET", "GetStampAndSignature", idDocument=id_document)
        else:
            response = self.safe_request("GET", "GetStampAndSignature", signature=signature)
        return response['Code'], response['Stamp'] if response['Code'] == 0 else response['ErrorMessage']

    def get_projects_by_document(self, id_document=None, signature=None):
        if not (id_document or signature):
            raise PiTreException(f"PiTre: missing document id or signature in GetProjectsByDocument")
        response = None
        if id_document:
            response = self.safe_request("GET", "GetProjectsByDocument", idDocument=id_document)
        else:
            response = self.safe_request("GET", "GetProjectsByDocument", signature=signature)
        return response['Code'], response['Projects'] if response['Code'] == 0 else response['ErrorMessage']

    def safe_request(self, method, route_name, data=None, is_token_required=True, **filters):
        headers = {
            "CODE_ADM": self.code_adm,
            "ROUTED_ACTION": route_name,
            "AuthToken": self.get_token() if is_token_required else None,
            "Content-Type": "application/json"
        }

        attempt = 0
        url = self.api_url + (f"?{urlencode(filters)}" if filters else "")

        # Retry request for #attempts
        while attempt < self.attempts:
            logger.debug(f"PiTre: {attempt + 1 }/{self.attempts} Sendind {method} {url} request {'with data ' + json.dumps(data, indent=2) if data else ''}")
            attempt += 1
            try:
                response = requests.request(
                    method=method,
                    url=url,
                    headers=headers,
                    data=json.dumps(data),
                    cert=self.certificate,
                    verify=False,
                    timeout=(self.connection_timeout, self.read_timeout)
                )
            except requests.exceptions.ConnectionError:
                logger.error(f"PiTre: unable to connect to url {url} after {attempt} attempts")
            except requests.exceptions.ReadTimeout:
                logger.error(f"PiTre: unable to read from url {url} after {attempt} attempts")
            except Exception as ex:
                logger.error(f"PiTre: An error occurred from url {url} after {attempt} attempts: {str(ex)}")
            else:
                if response.ok:
                    return response.json()
                else:
                    logger.error(f"SDC: {response.status_code} from {url} after {attempt} attempts:\n{response.text}")
                    sleep(pow(self.sleep_seconds, attempt))
        raise PiTreException(f"PiTre: cannot complete {method} {url} after {attempt} attempts {'with data ' + json.dumps(data, indent=2) if data else ''}")
