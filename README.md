# Stanzadelcittadino PiTre Service - DEPRECATO

Questo servizio non è più mantenuto, superato da un progetto successivo [Application Registry](https://gitlab.com/opencity-labs/area-personale/stanzadelcittadino-application-registry)

## Installation

### Install virtualenv

    virtualenv venv --python python3.7
    source venv/bin/python
    
### Install dependencies

    pip install -r requirements.txt
    
### Execution

    python -W ignore protocol.py
    
## Configuration

For the general configuration it is necessary to define some parameters in the file `config/config.json` (see `config/config.json.dist`).
t is possible to subdivide these parameters according to the environment used (default: test)

### Stanza del cittadino


Here are some parameters required for configuring the SDC class:

|   Name        |   Description                                     |
|---------------|---------------------------------------------------|
|   `api_url`   |   URL address to the SDC APIs                     |
|   `username`  |   username used for the SDC APIs authentication   |
|   `password`  |   password used for the SDC APIs authentication   |
|   `logo_url`  |   SDC tenant logo url                             |
|   `tenant`    |   SDC tenant name                                 |

### P.I.Tre

Here are some parameters required for configuring the Pitre class

|   Name                            |   Description                                                                                 |
|-----------------------------------|-----------------------------------------------------------------------------------------------|
|   `api_url`                       |   PiTre API base url                                                                          |
|   `certificate`                   |   Certificate used to get the token used in PiTre REST requests                               |
|   `username`                      |   Username used to get the token used in PiTre REST requests                                  |
|   `code_role`                     |   Role used to get the token used in PiTre REST requests                                      |
|   `code_application`              |   Application code that interfaces to Pitre                                                   |
|   `code_adm`                      |   Administration code                                                                         |
|   `code_register`                 |   Register code                                                                               |
|   `code_rf`                       |   RF code for the signature                                                                   |
|   `code_node_classification`      |   Node of the holder in which to create the project                                           |
|   `transmission_project_model`    |   List of IDs of the transmission models used to transmit the project                         |
|   `transmission_document_model`   |   List of IDs of the transmission models used to transmit the document                        |
|   `receiver_code`                 |   Transmission receiver code if no transmission model is used                                 |
|   `transmission_reason`           |   Reason of transmission if no transmission model is used                                     |
|   `means_of_sending`              |   Transmission medium used in the creation of documents                                       |
|   `outcome_document_template_id`  |   ID of the template used to create outome protocol document if typed documents are enabled   |
|   `income_document_template_id`   |   ID of the template used to create income protocol document if typed documents are enabled   |
|   `project_template_id`           |   ID of the template used to create project if typed projects are enabled                     |
|   `address_book_rf`               |   RF used for the search of the correspondences                                               |
|   `default_sender`                |   Structure used as receiver of outbound protocols and sender in inbound protocols            |
|   `default_note`                  |   Default note used in outbound protocolled documents                                         |
|   `private_project`               |   Create private PiTre project                                                                |
|   `private_document`              |   Create private PiTre document                                                               |


#### NB: Default sender template:
default_sender is an object and MUST contain all filters required for the search_correspondents request:

       "default_sender": {
        "code_role": "123",
        "type": "INTERNAL|EXTERNAL|GLOBAL",
        "offices": "TRUE|FALSE",
        "roles": "TRUE|FALSE"
      }

## Environment variables

In addition to the configuration parameters described in the previous paragraph, it is possible to customize the 
execution of the script by defining some environment variables (See the `.env.dist` file):


| Name                    | Description                                                                                                                                                                                     | Default          |
|-------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------|
| `env`                   | Name of the environment used.                                                                                                                                                                   | `test`           | 
| `ERROR_SLEEP_SECONDS`   | Number of seconds to wait before the next iteration in case an error occurs during the protocol flow                                                                                            | `60`             |
| `SUCCESS_SLEEP_SECONDS` | Number of seconds to wait between two successive iterations in the the protocol flow                                                                                                            | `10`             |
| `REDIS_URL`             | Url of the redis server used                                                                                                                                                                    | ---              |
| `REDIS_KEY_EXPIRATION`  | Number of seconds of validity of a key saved in redis; if not specified, the keys will not expire                                                                                               | ---              |
| `GOTENBERG_URL`         | Url to the Gotengerg server used for generating the documents to be sent to the protocol                                                                                                        | ---              |
| `SAVE_PDFS`             | Set to `true` if you want to locally save all documents generated using Gotenberg. If enabled, all documents will be saved in the `pdf` directory                                               | `false`          |
| `SAVE_XMLS`             | Set to `true` if you want to locally save all signatures. If enabled, all documents will be saved in the `xmls` directory                                                                       | `false`          |
| `API_START_OFFSET`      | Initial offset used to request applications via API                                                                                                                                             | `0`              |
| `API_LIMIT`             | Number of results per page to be obtained for each API call                                                                                                                                     | `10`             |
| `API_ORDERING`          | Applications ordering filter                                                                                                                                                                    | `submissionTime` |
| `API_SORTING`           | Applications sorting filter                                                                                                                                                                     | `ASC`            |
| `API_VERSION`           | SDC Api version                                                                                                                                                                                 | `1`              |
| `SERVICES_WHITELIST`    | Service's slugs whitelist for which to filter the applications that must be sent to the protocol                                                                                                | ---              |
| `STATUSES_WHITELIST`    | Application statuses whitelist for which to filter the applications that must be sent to the protocol                                                                                           | ---              |
| `IDS`                   | Whitelist of the id of the practices that must be sent to the protocol **                                                                                                                       | ---              |
| `LIMIT`                 | Limit to the number of applications that must be protocolled in the current execution                                                                                                           | ---              |
| `SEARCH_CORRESPONDENT`  | Enables the search for the correspondent within the address books of the PiTre                                                                                                                  | `false`          |
| `CREATE_CORRESPONDENT`  | Enables the creation of a correspondent in the Pitre address book if it is not already present. If this feature is not enabled, an occasional correspondent will be created for each iteration. | `false`          |
| `CORRESPONDENT_CHANNEL` | Preferred channel used when creating a new correspondent (Ex: `MAIL`, `LETTERA`)                                                                                                                | `MAIL`           |
| `SEARCH_DOC_BY_YEAR`    | Search documents by year                                                                                                                                                                        | ---              |
| `SEARCH_PRJ_BY_YEAR`    | Search projects by year                                                                                                                                                                         | ---              |
| `TYPED_PROJECT`         | Enable the use of a typed project template                                                                                                                                                      | `false`          |
| `TYPED_DOCUMENT`        | Enable the use of a typed document template                                                                                                                                                     | `false`          |
| `TRANSMIT_DOCUMENT`     | Enable document transmission                                                                                                                                                                    | `false`          |
| `SEND_OUTCOME_DOCUMENT` | Enable document sending                                                                                                                                                                         | `false`          |
| `RECORD_STATUSES`       | Filter on application status messages that must be sent to the protocol                                                                                                                         | ---              |
| `RECORD_RESPONSES`      | Enables the sending of documents relating to the closure of an application to the protocol                                                                                                      | `true`           |
| `RECORD_NOTES`          | Enables the sending to the protocol of documents relating to internal communications sent during the life cycle of the application ***                                                          | `never`          |
| `RECORD_MESSAGES`       | Enables the sending to the protocol of documents relating to public communications sent during the life cycle of the application ***                                                            | `never`          |
| `UPDATE_SOURCE`         | Enable updating of protocol information on source system                                                                                                                                        | `false`          |
| `LOG_LEVEL`             | Log level used during the execution                                                                                                                                                             | `INFO`           |


** If an id whitelist is defined all further filters on the service and status will be ignored. It is therefore assumed 
that all the configurations necessary for the protocol of these applications are well defined within the `mapping.json` 
file


*** The options available for this configuration are `always`, `never`, `on-demand` (sending to the protocol is based 
on a specific flag defined within the message)

## Document configuration


It is possible to configure all the information sent to the protocol system starting from the description of the 
project to the description of each single document.

For this purpose, it is necessary to define in the `mapping.json` file all the configurations starting from the service 
to which the applications analyzed during the protocol flow.


The first level of this object is divided into two main keys:
*   `service_groups`: Contains general configurations of the service group
*   `services`: Contains precific configurations of the service

        {
            "service_groups": { ... },
            "services": { ... }
        }
Service group template:
    
    {
        "name": "Service name",
        "project": "Project description",
    }

Service template:

    {
        "name": "Service name",
        "group": "Service group slug",
        "project": "Project description",
        "document": "Main document description",
        "outcome": "Outcome document description",
        "message": "Message document description",
        "meta": {
            "CONSTANT": "constant field value",
            "COMPUTED": "%application.field_key%",
            ...
        }
    }

As anticipated, the configurations must be divided by service: in particular, the service slug will be the key of the 
dictionary containing the specific configuration.

If a service belongs to a group of services, it is sufficient to report the value of the group slug in the `group` key, 
in this way the `project` value of the group will be used for the construction of the project object, otherwise the 
value of the `project` field will be of a "standalone" service.

Service group

    {
        "service_groups": {
            ...
            "service_group_slug": {
                ...
                "project": "project description"
                ...
            }
            ...
        },
        "services": {
            "service_slug": {
                ...
                "gruop": "service_group_slug",
                "project": null
                ...
            }
            ...
        }
    }

Service standalone

    {
        ...
        "services": {
            "service_slug": {
                ...
                "gruop": null,
                "project": "project description"
                ...
            }
            ...
        }
    }
    

The `document_object`, field will instead be used to construct the object of the document sent to the 
protocol.

The meta object is used to populate the fields of the typed project/document in case the functionality is enabled.

Each textual field can be enriched using placeholders whose value will be replaced at runtime with the values present 
within the application or within the form data: just write the name of the key surrounded by the '%' character

    "meta": {
            "CONSTANT": "constant field value",
            "COMPUTED": "%application.field_key%",
            ...
        }

What has been defined up to now is used to define the general parameters of the project or of the document, it is 
possible to override these settings depending on whether the document is classified as `document`, `outcome`, `user_message` 
or `operator_message`.
These four types reflect the different types of communication that can take place on the "Citizen's Room" portal:
* `document`: It represents the main application that the citizen compiles
* `outcome`: It represents the final answer of the operator who is in charge of the application, that is the result 
document of the procedure
* `user_message`: Represents citizen communications presented in the form of messages
* `operator_message`: Represents the communications sent by the operators of the administration on which the 
request was submitted


In order to be able to overwrite the parameters mentioned above it is sufficient to overwrite the individual fields:

    "services": {
        "service_slug": {
            "document_object": "General document object",
            "meta": {
                "Field1": "A",
                "Field2": "B"
            }
            ...
            "outcome": {
                "document_object": "Specific document object for outcome documents",
                "meta": {
                    "Field1": "1",
                    "Field3": "3"
                }
            }
        },
        ...
    }


As it is easy to see in the example it is not only possible to overwrite the parameters already defined in the meta 
block, but it is also possible to add new specific values for the typed document type

CONVENTION: 
- In order to obtain the year of the application, use the placeholder `%application.year%`
- In order to obtain the message datetime, use the placeholder `%message.datetime%`

### Correspondent customization

It is possible to customize the information of a correspondent in a similar way to what has already been seen for the 
object of the document and of the project. 

#### Occasional correspondent
By default, for an occasional correspondent, the description is composed as follows:
    
    SURNAME NAME - FISCAL CODE

You can change the default by adding the `correspondent` object in the service configuration defined in the mapping file:

     "services": {
        "service_slug": {
            "document_object": "Document object",
            ...
            "correspondent": {
                "description": "New description for occasional correspondent"
            }
        },
        ...
    }

#### Correspondent in the address book

As for non-occasional correspondents, it is possible to customize all the fields listed below, with the same logic 
used for the different configurations:

|   Name                |   Default value               |
|-----------------------|-------------------------------|
|   `description`       |   SURNAME NAME - FISCAL CODE  |
|   `name`              |   NAME                        |
|   `surname`           |   SURNAME                     |
|   `fiscal_code`       |   FISCAL CODE                 |
|   `code`              |   FISCAL CODE                 |
|   `phone_number`      |   ---                         |
|   `phone_number2`     |   ---                         |
|   `email`             |   ---                         |
|   `other_emails`      |   ---                         |
|   `address`           |   ---                         |
|   `cap`               |   ---                         |
|   `city`              |   ---                         |
|   `province`          |   ---                         |
|   `location`          |   ---                         |
|   `nation`            |   ---                         |
|   `fax`               |   ---                         |
|   `vat_number`        |   ---                         |
|   `note`              |   ---                         |

Example:
    
    "services": {
        "service_slug": {
            "document_object": "Document object",
            ...
            "correspondent": {
                "description": "New description for occasional correspondent",
                "phone_number": "%applicant.data.cell_number%",
                "email": "%applicant.data.email_address%",
                "address": "%applicant.data.address.data.address%",
                "cap": "%applicant.data.address.data.postal_code%",
                "city": "%applicant.data.address.data.municipality%",
            }
        },
        ...
    }

## P.I:Tre available functions

The PiTRE class provides various methods that interface with pitre's REST APIs:

*   `get_token`:  Method for generating the authentication token to be inserted in subsequent requests, in a header
*   `get_default_sender`: Retrieves default sender from the address book
*   `set_active_classification_scheme`: Method for collecting the active holder, within which new projects can be created
*   `create_project`: Method that allows the creation of a project.
*   `get_transmission_models`:  Method for finding all transmission models for documents or projects.
*   `execute_transm_prj_model`: Method for transmitting a project using a transmission model.
*   `get_registers_or_rf`:  Method used to obtain the detail of the registers or RF available for a role.
*   `get_users_in_role`:    Method for picking up the users present in a given role, starting from the role code.
*   `get_project`:  Service for retrieving the data of project given a code or id.
*   `upload_file_to_document`:  Method to add a file to a document, to create an attachment or to add a new version of the file.
*   `search_correspondence`:    Method for searching for correspondents, by inserting the filters available in the request through the `GetCorrespondentFilters` method
*   `search_projects`:  Service that allows the search of projects
*   `search_document`:  Method for searching the documents
*   `get_role`: Method for retrieving the details of a role given the role code
*   `get_correspondence`:   Method that returns the list of filters applicable to the correspondences search.
*   `get_project_filters`:  Method that returns the list of filters applicable to the projects search.
*   `create_document`:  Method for creating a document
*   `get_document`: Method for finding the detail of a document given the id.
*   `edit_document`:    Method for editing a document or protocol. The request is in the same form as `CreateDocument`.
*   `add_doc_in_project`:   Method for inserting a document into a project.
*   `execute_transmission_project`: Method for the single transmission of a project without the use of a transmission model.
*   `protocol_predisposed`: Method for registering a document prepared without modifying further data
*   `...`


## Protocol flow

The registration flow takes place mainly in two phases: 
*   retrieval of the documents to be sent to the protocol system, 
*   effective registration of the documents:

#### Retrieval

The recovery of the documents to be registered takes place using the SDC API:
For each application analyzed, a check is made to verify whether the logging is required or not (service, status, id) 
and if it is necessary to log the messages sent.
Once all the "valid" applications have been obtained, all the necessary document types are generated on the basis of the 
current configuration: main document, outcome and message.

The documents thus generated contain all and only the information necessary to proceed with the actual registration.

    {
        "project_object": "",
        "document_object": "",
        "correspondent_completename": "",
        "correspondent_name": "",
        "correspondent_surname": "",
        "correspondent_fiscal_code": "",
        "correspondent_email": "",
        "correspondent_phone_number": "",
        "document": {
            "name": "",
            "url": "",
            "original_name": ""
        },
        "attachments": [],
        "meta": {
          "external_key": "a0f97f65-24e7-43ef-b161-9ca46d15de12",
          ...
        },
        "type": "IN",
        "arrival_date": "",
        "source_type": "application",
        "source_url": ""
  },

The flow of protocollation of the applications takes place through a series of steps described here:

#### Protocol

For each document obtained from the previous phase, the registration takes place according to the following phases:

##### Redis check

The application uses a redis server to save the information relating to the documents analyzed: a key is saved for each 
created or registered project/document, in order to skip unnecessary steps.

##### Project

The first step is to search for the project in which to insert the documents. If it is not found inside the PiTre, one 
is created and immediately transmitted to the competent role.

If typed collation is enabled, the keys contained in the meta object are used to populate the template fields.

##### Correspondent

Once the project has been created, the corresponding sender or recipient of the document must be created.
Through the available configurations it is possible to specify whether the correspondent should be an occasional 
correspondent, whether it should be searched in the PiTre address book and if not, whether it should be added.

##### Document

At this point a new document is created if the search does not produce any results. As in the case of the project, it 
is possible to enable the use of a typed document: the search and eventual creation of the document will use the same 
metadata to populate the template fields.

The following steps are:
- Loading the main document
- Upload of attachments

In both cases, it is checked that the files have not already been loaded into the PiTre document in order to avoid 
duplicates. This check is done on the basis of the file name.

The retrieval of the attachment can take place through a remote url to the resource or through the generation of the 
pdf at runtime, through a predefined template (this happens in the case of documents relating to messages, both public 
and internal)

##### Protocol

The final step of the process involves registering the document obtained in the previous steps, at the end of which a 
protocol number and signature will be generated. Before the actual registering, it is checked that the document does not 
already have a protocol number and a signature.

Once registered, the document will be added to the previously obtained project.

### Signature

If the data relating to the user's session is available, the "signature.xml" file will be generated and registered.

The signature file will be different depending on whether the citizen has accessed the portal through spid or cps/cns: 
two examples are shown below

##### CPS/CNS


    <?xml version="1.0" encoding="utf-8"?>
    <istanzeonline>
        <serviceprovider value="CPS/CNS">
            <certificationauthority>CN=Provincia autonoma Trento - CA Cittadini, OU=Servizi di Certificazione, O=Actalis S.p.A., C=IT</certificationauthority>
            <codicefiscale>AAAAAA00A00A000A</codicefiscale>
            <cognome>COGNOME</cognome>
            <nome>NOME</nome>
            <dataautenticazione>19/11/2020</dataautenticazione>
            <moduloId>STANZA DEL CITTADINO / RICHIESTA XXX</moduloId>
            <oraautenticazione>09:34</oraautenticazione>
            <autenticationmethod>SMARTCARD</autenticationmethod>
            <hashdocumento>7ce18fe376074aacfdcd378bc17890b324a84c9fe0a165a8a50d170b3c7bd935</hashdocumento>
        </serviceprovider>
    </istanzeonline>

##### SPID


    <?xml version="1.0" encoding="utf-8"?>
    <istanzeonline>
        <serviceprovider value="SPID">
            <codicefiscale>AAAAAA00A00A000A</codicefiscale>
            <cognome>COGNOME</cognome>
            <nome>NOME</nome>
            <dataautenticazione>29/12/2020</dataautenticazione>
            <moduloId>STANZA DEL CITTADINO / RICHIESTA XXX</moduloId>
            <oraautenticazione>13:34</oraautenticazione>
            <spidcode>XXX</spidcode>
            <autenticationmethod>OTP</autenticationmethod>
            <hashdocumento>7ce18fe376074aacfdcd378bc17890b324a84c9fe0a165a8a50d170b3c7bd935</hashdocumento>
        </serviceprovider>
    </istanzeonline>



### Output

For each document analyzed a line will be populated in a csv produced by the script:

An example:

    1aaa1111-111a-1aaa-aa11-a1a1a11a1111,99-2020-9,SERVICE GROUP - Richiedente SURNAME NAME (FISCAL CODE),123456789,SERVICE GROUP - SERVICE NAME - Richiedente SURNAME NAME (FISCAL CODE) - APPLICATION_ID,1234,ADM/RFS123-06/10/2020-1234567,

In order we have:
        
*   `application_id`
*   `project_code`
*   `project_description`
*   `document_id`
*   `document_description`
*   `protocol_number`
*   `signature`
*   `error`
